DROP DATABASE StoredPr_DB;
CREATE DATABASE IF NOT EXISTS StoredPr_DB
CHARACTER SET utf8 
COLLATE utf8_general_ci;

USE StoredPr_DB; 

CREATE TABLE employee(
    id                 INT               AUTO_INCREMENT,
    surname            VARCHAR(30)       NOT NULL,
    name               CHAR(30)          NOT NULL,
    midle_name         VARCHAR(30),
    identity_number    CHAR(10),
    passport           CHAR(10),
    experience         DECIMAL(10, 1),
    birthday           DATE,
    post               VARCHAR(15)       NOT NULL,
    pharmacy_id        INT,
    PRIMARY KEY (id)
)ENGINE=INNODB;

CREATE TABLE medicine(
    id               INT            AUTO_INCREMENT,
    name             VARCHAR(30)    NOT NULL,
    ministry_code    CHAR(10),
    recipe           BIT(1),
    narcotic         BIT(1),
    psychotropic     BIT(1),
    PRIMARY KEY (id)
)ENGINE=INNODB;

CREATE TABLE medicine_zone(
    medicine_id    INT    NOT NULL,
    zone_id        INT    NOT NULL,
    PRIMARY KEY (medicine_id, zone_id)
)ENGINE=INNODB;

CREATE TABLE pharmacy(
    id                 INT            AUTO_INCREMENT,
    name               VARCHAR(15)    NOT NULL,
    building_number    VARCHAR(10),
    www                VARCHAR(40),
    work_time          TIME,
    saturday           BIT(1),
    sunday             BIT(1),
    street             VARCHAR(25),
    PRIMARY KEY (id)
)ENGINE=INNODB;

CREATE TABLE pharmacy_medicine(
    pharmacy_id    INT    NOT NULL,
    medicine_id    INT    NOT NULL,
    PRIMARY KEY (pharmacy_id, medicine_id)
)ENGINE=INNODB;

CREATE TABLE post(
    post    VARCHAR(15)    NOT NULL,
    PRIMARY KEY (post)
)ENGINE=INNODB;

CREATE TABLE street(
    street    VARCHAR(25)    NOT NULL,
    PRIMARY KEY (street)
)ENGINE=INNODB;

CREATE TABLE zone(
    id      INT            AUTO_INCREMENT,
    name    VARCHAR(25)    NOT NULL,
    PRIMARY KEY (id)
)ENGINE=INNODB;

DELIMITER //
CREATE PROCEDURE employee_insertion ( -- this procedure insert new row in employee table
	IN name_in VARCHAR(30),
    IN surname_in VARCHAR(30),
    IN middle_name VARCHAR(30),
    IN id_number int(10),
    IN pasport_number CHAR(10),
    IN experience DECIMAL(10,1),
    IN birthday DATE
    )
BEGIN
	INSERT INTO employee VALUES (DEFAULT, name_in, surname_in, middle_name, id_number, pasport_number, experience, birthday, 'лікар-продавець', NULL);
END//
DELIMITER ;

-- DROP PROCEDURE insert_influence;
DELIMITER //
CREATE PROCEDURE insert_influence (  -- this procedure adds new dependencies in join table medicine_zone
	IN medicine_id int,
    IN zone_id int
	)
BEGIN
	IF medicine_id IN (SELECT id FROM medicine ) AND zone_id  IN (SELECT id FROM zone)
    THEN INSERT INTO medicine_zone VALUES (medicine_id, zone_id);
    ELSE SELECT 'invalid input' FROM medicine;
    END IF;
END//
DELIMITER ;

--  DROP PROCEDURE cursor_example;
DELIMITER //
CREATE PROCEDURE cursor_example() -- simple cursor , creates tables for all employyes (without RAND)
BEGIN 
	DECLARE done INTEGER DEFAULT 0;
    DECLARE surnameC VARCHAR (30);
    
    DECLARE cursor_value CURSOR
    FOR SELECT surname FROM employee;
    
    DECLARE CONTINUE HANDLER 
    FOR NOT FOUND SET done = 1;
    
    OPEN cursor_value;
    myLoop: LOOP
			FETCH cursor_value INTO surnameC;
            IF done = 1 THEN LEAVE myLoop;
            END IF;
            SET @temp_query = CONCAT('CREATE TABLE ', surnameC, ' (id INTEGER, name varchar(20))');
            PREPARE myquery FROM @temp_query;
            EXECUTE myquery;
            DEALLOCATE PREPARE myquery;
            END LOOP;
            CLOSE cursor_value;
END//
DELIMITER ;
-- DROP FUNCTION minExpOfEMPL;
DELIMITER //
CREATE FUNCTION minExpOfEmpl() RETURNS DECIMAL(10,1) DETERMINISTIC -- this function finds minimal exprerience in employee table
BEGIN
	RETURN (SELECT MIN(experience) FROM employee);
END//

DELIMITER ;
-- DROP TRIGGER drugInsertionFormat;
DELIMITER //
CREATE TRIGGER drugInsertionFormat -- this trigger controls format of ministry-code
BEFORE INSERT
ON medicine FOR EACH ROW
BEGIN
	IF (new.ministry_code NOT RLIKE '^[^М^П]{2}-[0-9]{3}-[0-9]{3}')
    THEN SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'wrong format of ministry_code';
	END IF;
END//
DELIMITER ;

-- DROP TRIGGER idNumberFormat;
DELIMITER //
CREATE TRIGGER idNumberFormat -- this Trigger check identity_number format
BEFORE INSERT 
ON employee FOR EACH ROW
BEGIN
	IF (new.identity_number  RLIKE '00$')
    THEN SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'wrong format of identity_number';
	END IF;
END//
DELIMITER ;
 
DROP TRIGGER restrictionOfUpdate;
DELIMITER //
CREATE TRIGGER restrictionOfUpdate
BEFORE UPDATE
ON post FOR EACH ROW
BEGIN
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'restriction of updating';
END//
DELIMITER ;