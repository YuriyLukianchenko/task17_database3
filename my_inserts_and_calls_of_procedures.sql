
INSERT INTO street VALUES 	('Dovzhenka'),
							('Проспект Свободи'),
                            ('Зелена'),
                            ('Kytayska');
                            
INSERT INTO post VALUES ('лікар-продавець'),
						('продавець');
                        
INSERT INTO zone VALUES (1, 'heart'),
						(2, 'legs'),
                        (3, 'brain'),
                        (4, 'liver');
					
INSERT INTO medicine VALUES (1, 'tablet1', '123456789A', 1, 0, 0),
						(2, 'tablet2', '123456789B', 1, 1, 1),
                        (3, 'tablet3', '123456789C', 0, 0, 0),
                        (4, 'tablet4', '123456789D', 1, 0, 1),
                        (5, 'tablet5', '123456789E', 1, 1, 0);
                        
INSERT INTO pharmacy VALUES (DEFAULT, 'Anna', 4, 'apteka@gmail.com', '06:00:00', 1, 1, 'Зелена'),
							(DEFAULT, 'Health+', 24, 'Health@gmail.com', '08:00:00', 1, 1, 'Kytayska');
                            
INSERT INTO employee VALUES (DEFAULT, 'Petrenko', 'Jhon', 'Ivanovych', '1234567891', 'VK-1010101', 21.1, '1956-12-23', 'лікар-продавець', 1),
							(DEFAULT, 'Stocker', 'Anna', 'Petrivna', '1234567892', 'VK-1010102', 34.1, '1978-12-23', 'продавець', 1);
                            

INSERT INTO pharmacy_medicine VALUES (1,1), 
									(1,2),
                                    (1,3),
                                    (2,1),
                                    (2,4),
                                    (2,5);
                                    
INSERT INTO medicine_zone VALUES 	(1,1),
									(1,2),
                                    (1,3),
                                    (2,3),
                                    (3,1),
                                    (4,1),
                                    (4,3),
                                    (4,4),
                                    (5,4);
                                    


CALL employee_insertion('Bob','Jax', 'Stepanovich', 0987654321, 'SD-1234567', 34.2, '1975-02-23');
CALL insert_influence(1,4);
CALL cursor_example();
INSERT INTO medicine VALUES (DEFAULT, 'tablet1', 'DФ-123-516', 1, 0, 0);
INSERT INTO employee VALUES (DEFAULT, 'Petrenko', 'Jhon', 'Ivanovych', '234567801', 'VK-1010101', 21.1, '1956-12-23', 'лікар-продавець', 1);
UPDATE post SET post = 'бобер' WHERE post = 'продавець';